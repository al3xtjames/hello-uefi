# Makefile for helloworld-efi
# Based off macosxbootloader-clang Makefile by Pike R. Alpha - special thanks to him & Andy Vandijck!

PROJECT_DIR = $(PWD)

CC = /usr/bin/clang
LD = /usr/bin/ld
MTOC = /usr/local/bin/mtoc -subsystem UEFI_APPLICATION -align 0x20
STRIP = /usr/local/bin/strip -S

# Set the default target arch to 64-bit
ARCH = x86_64
ARCHDIR = X64
ARCHFLAGS = -arch x86_64
ARCHCFLAGS = -target x86_64-pc-win32-macho -funsigned-char -fno-ms-extensions -fno-stack-protector -fno-builtin -fshort-wchar -mno-implicit-float -msoft-float -mms-bitfields -ftrap-function=undefined_behavior_has_been_optimized_away_by_clang -D__x86_64__=1

# Include the EDK2 directories
INCLUDES = -I '$(PROJECT_DIR)/edk2/include' -I '$(PROJECT_DIR)/edk2/include/$(ARCHDIR)'

WFLAGS = -Wall -Werror -Wno-unknown-pragmas
CFLAGS = $(ARCHFLAGS) $(WFLAGS) $(ARCHCFLAGS) $(INCLUDES) -g0 -fno-standalone-debug -Oz -fapple-pragma-pack -fpie -nostdinc -fno-exceptions -std=gnu11
LDFLAGS = $(ARCHFLAGS) -u _EfiMain -e _EfiMain -preload -segalign 0x20 -pie -all_load -dead_strip -image_base 0x400 -flat_namespace -print_statistics -sectalign __TEXT __text 0x20 -sectalign __TEXT __eh_frame  0x20 -sectalign __TEXT __ustring 0x20 -sectalign __TEXT __const 0x20 -sectalign __TEXT __ustring 0x20 -sectalign __DATA __data 0x20 -sectalign __DATA __bss 0x20 -sectalign __DATA __common 0x20

all: clean hello.efi

hello.efi: hello.o
	@echo "Compiling hello.efi"
	@echo "--------------------"
	@echo "\t[CLANG]\thello.o";
	@$(CC) $(CFLAGS) -c hello.c -o hello.o
	@echo "\t[LD]\thello.sys";
	@$(LD) $(LDFLAGS) hello.o -map hello.map -o hello.sys
	@echo "\t[STRIP]\thello.sys"
	@$(STRIP) hello.sys
	@echo "\t[MTOC]\thello.sys hello.efi"
	@$(MTOC) hello.sys hello.efi
	@echo "\nCompilation finished. Output: $(PROJECT_DIR)/hello.efi"

clean:
	@rm -f *.efi *.o *.map *.sys
