# Hello World in UEFI

A simple example of building UEFI executables on OS X.

Build Instructions
------------------
For building, the latest Xcode Command Line Tools are required, along with `mtoc` (part of `cctools`, which can be installed with `brew install cctools && brew link --force cctools`).

Then, build it with `make` (note that the Makefile only supports targetting 64-bit UEFI).

The compiled UEFI binary will be output to hello.efi in the project folder. It can then be loaded from a UEFI shell, such as the one included with EDK2.
